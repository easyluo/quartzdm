//
//  QuartzDMPlugIn.h
//  QuartzDM
//

#import <Quartz/Quartz.h>

#define STATUS_OK  0
#define STATUS_NOCODE -1
#define ERR_DECODE 3

@interface QuartzDMPlugIn : QCPlugIn
{
  int skipper;
  NSUInteger skipFrames;
  NSUInteger maxRegionFind;
  unsigned char *pxl;
}

@property(assign) id<QCPlugInInputImageSource> inputImage;
@property(assign) NSString* outputString;
@property(assign) NSUInteger outputCode;
@property(assign) BOOL outputDetection;
@property(assign) NSUInteger skipFrames;
@property(assign) NSUInteger maxRegionFind;

@end
