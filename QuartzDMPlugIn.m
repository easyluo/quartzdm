//
//  QuartzDMPlugIn.m
//  QuartzDM
//

#import <OpenGL/CGLMacro.h>
#include "dmtx.h"

#import "QuartzDMPlugIn.h"
#define    kQCPlugIn_Name @"DataMatrixDecoder"
#define    kQCPlugIn_Description @"Decodes DataMatrix 2D Barcodes v1.1"

@implementation QuartzDMPlugIn

@dynamic inputImage, outputString, outputCode, outputDetection;
@synthesize skipFrames;
@synthesize maxRegionFind;

+ (NSArray*) plugInKeys
{
    return [NSArray arrayWithObjects: @"skipFrames",
                @"maxRegionFind",
                nil];
}

- (QCPlugInViewController*) createViewController
{
    return [[QCPlugInViewController alloc]
            initWithPlugIn:self
            viewNibName:@"QuartzDMSettings"];
}

+ (NSDictionary*) attributes
{
	return [NSDictionary dictionaryWithObjectsAndKeys:
				kQCPlugIn_Name, QCPlugInAttributeNameKey,
                kQCPlugIn_Description, QCPlugInAttributeDescriptionKey,
                nil];
}

+ (NSDictionary*) attributesForPropertyPortWithKey:(NSString*)key
{
    if([key isEqualToString:@"inputImage"])
        return [NSDictionary dictionaryWithObjectsAndKeys:
                @"Image", QCPortAttributeNameKey,
				QCPortTypeImage,QCPortAttributeTypeKey,
                nil];
	if([key isEqualToString:@"outputDetection"])
        return [NSDictionary dictionaryWithObjectsAndKeys:
                @"Detected", QCPortAttributeNameKey,
				QCPortTypeBoolean,QCPortAttributeTypeKey,
                nil];
    if([key isEqualToString:@"outputString"])
        return [NSDictionary dictionaryWithObjectsAndKeys:
                @"Decoded", QCPortAttributeNameKey,
				QCPortTypeString,QCPortAttributeTypeKey,
                nil];
    if([key isEqualToString:@"outputCode"])
        return [NSDictionary dictionaryWithObjectsAndKeys:
                @"StatusCode", QCPortAttributeNameKey,
				QCPortTypeNumber,QCPortAttributeTypeKey,
                nil];			
	return nil;
}

+ (QCPlugInExecutionMode) executionMode
{	
	return kQCPlugInExecutionModeProvider;
}

+ (QCPlugInTimeMode) timeMode
{	
	return kQCPlugInTimeModeTimeBase;
}

- (id) init
{
	if(self = [super init]) {
		self.skipFrames = 48;
		self.maxRegionFind = 100;
	}
	
	return self;
}

- (void) finalize
{	
	[super finalize];
}

- (void) dealloc
{
	[super dealloc];
}

@end

@implementation QuartzDMPlugIn (Execution)

- (BOOL) startExecution:(id<QCPlugInContext>)context
{
    skipper = 0;
    return YES;
}

- (void) enableExecution:(id<QCPlugInContext>)context
{
}

- (BOOL) execute:(id<QCPlugInContext>)context atTime:(NSTimeInterval)time withArguments:(NSDictionary*)arguments
{

	NSRect imageRect;
		
	DmtxImage    *img;
	DmtxDecode   *dec;
	DmtxRegion   *reg;
	DmtxMessage  *msg;
	
	if(skipper++ == self.skipFrames) {
	   //NSLog(@"Not skipped.");
	   skipper=0;
	} else {
    	return YES;
	}

		if (self.inputImage != nil)
		{
            CGColorSpaceRef colorSpace = [self.inputImage imageColorSpace];
   
            //Intel 
            NSString* format = QCPlugInPixelFormatBGRA8;

			imageRect  = [self.inputImage imageBounds];
			if(![self.inputImage lockBufferRepresentationWithPixelFormat:format
									colorSpace:colorSpace 
                                    forBounds:imageRect]) {
                return NO;
            }
			//Allocate memory
            if(pxl == NULL) {
			  pxl = (unsigned char *)malloc(3 * imageRect.size.width * imageRect.size.height * sizeof(unsigned char));
			}
            img = dmtxImageCreate(pxl, imageRect.size.width, imageRect.size.height, DmtxPack24bppBGR);
			unsigned char *bufpxl  = (unsigned char *)[self.inputImage bufferBaseAddress];
		    
			//NSLog(@"Buffer attributes: components=%lu bpr=%d high=%d wide=%d",CGColorSpaceGetNumberOfComponents(colorSpace),[self.inputImage bufferBytesPerRow],[self.inputImage bufferPixelsHigh],[self.inputImage bufferPixelsWide]);
			
			int sizepix = img->width * img->height;
			
			int i;

			unsigned char *imgpxl = img->pxl;
			
			for(i = 0; i < sizepix; i++) 
			{ 
			  bufpxl++;                    //skip alpha 
			  memcpy(imgpxl,bufpxl,3);     //copy pixels
			  bufpxl++,bufpxl++,bufpxl++;  //skip copied pixels
			  imgpxl++;                    //next triple
			  
		    }
            [self.inputImage unlockBufferRepresentation];
			dec = dmtxDecodeCreate(img, 1);
			//NSLog(@"Init decode Struct.",dec);
            DmtxTime t = dmtxTimeNow();
			t = dmtxTimeAdd(t, self.maxRegionFind);
            reg = dmtxRegionFindNext(dec, &t);
            //NSLog(@"Region search returned %d.",reg);
		
			
			if(reg == NULL) {
				self.outputString =@"";
				self.outputCode = STATUS_NOCODE;
				self.outputDetection = NO;
				
				dmtxRegionDestroy(&reg);
				dmtxDecodeDestroy(&dec);
				dmtxImageDestroy(&img);
				return YES;
			}
			
			//NSLog(@"Attempting decode.");
			msg = dmtxDecodeMatrixRegion(dec, reg, -1);
            if(msg != NULL) {
			   self.outputString = [[[NSString alloc] initWithUTF8String:(const char*)msg->output] autorelease];
			   self.outputDetection = YES;
			   self.outputCode = STATUS_OK;
               dmtxMessageDestroy(&msg);
            } else {
			  self.outputCode = ERR_DECODE;
		      self.outputString =@"";
			  self.outputDetection = NO;
			}
			
			dmtxRegionDestroy(&reg);
			dmtxDecodeDestroy(&dec);
			dmtxImageDestroy(&img);
		}
	//} 
	return YES;
}

- (void) disableExecution:(id<QCPlugInContext>)context
{
    if(pxl != NULL) {
        free(pxl);
        pxl = NULL;
    }
}

- (void) stopExecution:(id<QCPlugInContext>)context
{
    if(pxl != NULL) {
        free(pxl);
        pxl = NULL;
    }
}

@end
